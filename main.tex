\documentclass[11pt, a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{eurosym}
\usepackage[hidelinks]{hyperref}
\usepackage[margin=3cm]{geometry}
\widowpenalty=10000
\clubpenalty=10000


\usepackage[colorinlistoftodos,prependcaption,textsize=scriptsize]{todonotes}

\begin{document}


\title{Open Software Landscape for Science Hardware Development in 2022}
\author{Hopefully lots of people}

\maketitle

\todo[inline]{Think of a way to track all authors not just git users}

\begin{abstract}
TODO
\end{abstract}


\section{Introduction}
Scientific research relies on scientists being able to replicate and build upon the works of other. Over the recent years there has been a push to increase the replicability of science. Rather than only publishing a paper about their work, scientists are encouraged or even required to openly share the raw data they have collected, and the software used to generate and analyse the data. Scientific instruments are essential for experimental science, their design and improvement drives new discoveries. The design of a new instrument is arguably more important to replicability in science than the raw data it produces. Complete, well-documented, open instrument designs allows scientists across the globe to inspect instruments inner workings, replicate experiments with precision, to collaborate remotely on cutting edge instrumentation, and adapt the design for their own needs.

Open instrument designs technically do not require open source software. However, using proprietary software can often limit the audience that are able to inspect, adapt, and replicate a design. Even software provided at no-cost can limit participation, for example academic licenses exclude community scientists. Cloud services can leave a project vulnerable, as how can you create a permanent archive of your work when it can only be used via a platform that may disappear, or change its licencing?

The decision of whether to use open source tools for open instrumentation requires a level of pragmatism. If no open source alternative is available that replicates the features of proprietary tools, researchers rarely have the time, expertise, ability, or funding to write this software themselves. For greater replicability of science funders should invest in the open source software toolchain needed to ensure that designs of scientific instruments can be transferred freely and can be archived in an editable and open standard format.

This manuscript seeks to describe the current state of the open source software landscape for scientific instrument design. It identifies strengths and weaknesses of current software, gaps where no software is available, young projects that are addressing gaps, and work-arounds that can be used to keep an instrument design as open as possible. This software landscape is rapidly advancing, as such this paper will remain and open repository to be updated periodically.

\section{Overview of Software Landscape for Scientific Hardware Design}

\subsection{Mechanical CAD}

Topics still to cover in prose
\begin{itemize}
 \item What is CAD and why it is important
 \item Editable CAD files vs exports (STL/STEP). Issues due to lack of standardisation
 \item Comment on proprietary CAD, lack of interoperability. Mention no-cost options, and the risk of bait and switch.
 \item Graphical CAD (FreeCAD), pros cons
 \begin{itemize}
  \item Very complete sketcher/part design, but toponaming issues remain
  \item Fast improving CAM/FEM features
  \item Technical drawings fast improving but still lack essentials such as hole callouts
  \item No consensus on assemblies (A2Plus, Assembly 3, Assembly 4) - Biggest issue for instrumentation. Are designs future proof?
 \end{itemize}
\end{itemize}

\subsubsection{Programatic CAD}

Another approach to generating 3-D design files for use in 3-D printing or CNC milling is the use of programmatic CAD like OpenSCAD and CADquery. Rather than draw objects as is done in conventional CAD, these types of open source programs enable users to write computer code to generate 3-D objects and are a preferred method for those with coding experience.  	OpenSCAD is a powerful parametric script-based CAD package that can be used for both research \cite{Pearce_2013, Baden_Chagas_Gage_Marzullo_Prieto-Godino_Euler_2015}, and STEM education \cite{Chacon_Saenz_de_la_Torre_Sanchez_2018, Gonzalez-Gomez_Valero-Gomez_Prieto-Moreno_Abderrahim_2012, Chacon_Saenz_Torre_Diaz_Esquembre_2017}.  The parametric nature means that when a design is made all future problems in that set can be solved by users easily customizing the design. OpenSCAD has been recommended as a core tool for general open hardware design because it can be integrated with functionalities that depend on geometry (e.g. electrical resistance) \cite{Oberloier_Pearce_2018}. For example, OpenSCAD has been used to design everything from photometric systems for enzymatic nitrate quantification \cite{Wijnen_Petersen_Hunt_Pearce_2016} to open source LED controllers for arbitrary spectrum visual stimulation and optogenetics during 2-photon imaging \cite{Zimmermann_Maia_Chagas_Bartel_Pop_Prieto-Godino_Baden_2020}.

It can also be used in various ways in a specific scientific line of inquiry such as with the exploration of the zebrafish, OpenSCAD was used to make a open-source platform for fluorescence microscopy, optogenetics, and accurate temperature control during behaviour of zebrafish \cite{Chagas_Prieto-Godino_Arrenberg_Baden_2017} as well as orientation tools for automated zebrafish screening assays \cite{Wittbrodt_Liebel_Gehrig_2014}.

Often scientists are relying on the geometry of an object to solve a problem that needs to be tested experimentally. A good example of this is the use of slot die processing of thin films. Slot dies have complicated internal geometries and are expensive to machine. By writing the design of the slot die in OpenSCAD many different internal geometries can be tested experimentally for 3-D printing rapidly at low costs \cite{Beeker_Pringle_Pearce_2018a}. Other examples of OpenSCAD being used for customizable design include mechanical optics equipment \cite{Zhang_Anzalone_Faria_Pearce_2013,Delmans_Haseloff_2018},  roller screws \cite{Guadagno_Loss_Pearce_2021}, random access plate storage \cite{Sanderson}, micromanipulators \cite{Hietanen_Heikkinen_Savin_Pearce_2018}, agrivoltaic testing systems \cite{Pearce_2021}, and portable microbiological mobile incubators and their associated attachments \cite{ Diep_Bizley_Ray_Edwards_2021}. OpenSCAD is also used extensively in microscopy \cite{Hohlbein_Diederich_Marsikova_Reynaud_Holden_Jahr_Haase_Prakash_2021}, with  both the OpenFlexure microscope \cite{Stirling_Sanga_Nyakyi_Mwakajinga_Collins_Bumke_Knapper_Meng_McDermott_Bowman_2020, Stirling_Bumke_Collins_Dhokia_Bowman_2021} as well as large 3D microscopes \cite{Wijnen_Petersen_Hunt_Pearce_2016} relying on it.

The parametric nature of OpenSCAD has also been used to create an open source syringe pump library \cite{Wijnen_Hunt_Anzalone_Pearce_2014} as well as many scientific devices that use them like pH-stat \cite{Milanovic_Milanovic_Kragic_Kostic_2018}, wax-based rapid protyper for microfluidics \cite{ Pearce_Anzalone_Heldt_2016}, open millifluidic \cite{ LeSuer_Osgood_Stelnicki_Mendez_2018} and macroscale scientific fluid handling and automation \cite{ Zhang_Wijnen_Pearce_2016}. 

Medical hardware also benefits from the parametric nature of OpenSCAD by allowing quick customization of prosthetic, particularly of growing children \cite{De_Maria_Di_Pietro_Ravizza_Lantada_Ahluwalia_2020}, medical devices that can be distributed manufactured during the COVID-19 pandemic when supply chains were broken \cite{Pearce_2020} like ventilators \cite{Petsiuk_Tanikella_Dertinger_Pringle_Oberloier_Pearce_2020, Oberloier_Gallup_Pearce_2022}, pulse oximeters \cite{Metcalfe_Iravani_Graham-Harper-Cater_Bowman_Stirling_Wilson_2021} or nasopharyngeal swabs \cite{Gallup_Pringle_Oberloier_Tanikella_Pearce_2020}. 
OpenSCAD has this parametric features built in and for best practices similar to coding it it recommended that designers list their variables at the front of their designs. For those less comfortable with code an open source 3-D model customizer is available to integrate into websites  \cite{Nilsiam_Pearce_2017}. 

OpenSCAD is mesh based, which makes it less useful for technical drawings, but is extremely power for additive manufacturing. Assemblies are possible in OpenSCAD, but are challenging and generally time consuming. In a study by Machado et al. that compared OpenSCAD and FreeCAD Python-based scripts they found that although Python for FreeCAD is more arduous to learn, its advantages include:  i) being able to export to standard parametric models; ii) using Python language with its libraries; and iii) the ability to use and integrate the models in its graphical interface counterbalance the initial difficulties \cite{Machado_Malpica_Borromeo_2019}.  That said, for ramping the average student up to being able to fabricate a complex 3-D printable geometry for scientific hardware, OpenSCAD is often the preferred tool even with makers and hobbyists \cite{West_Kuk_2016}.

Further points to be worked in later:
 \begin{itemize}
  \item CadQuery is built on a Boundary Representation (B-Rep) CAD kernel, has support for standard 3D and 2D construction functions (line, rectangle, extrude, hole cuts, etc), has support for 2D sketching with constraints, and supports assemblies with constraints.
 \end{itemize}


\subsection{Electronic CAD}

\todo[inline]{Need other expertise for this section}
Topics to cover in prose
\begin{itemize}
\item KiCAD
\end{itemize}

\subsection{Documentation}

Topics to cover in prose
\begin{itemize}
 \item Types of documentation needed
 \item Word processed/typeset documentation (Libre office,  \LaTeX)
 \item Markup documentation, and converstion to multiple formats (Markdown, PanDoc, Jekyll)
 \item Hardware specific documentation projects (GitBuilding)
\end{itemize}

\subsection{Software interfaces for instrumentation}

Topics to cover in prose
\begin{itemize}
 \item Back end - communication between instrumentation and program modules
 \begin{itemize}
  \item Busses - GPIB (old and poorly supported on some platforms), RS232 (well supported, polling, old, slow), USBTMC (just works on Linux, need more input on Windows), Ethernet (Best supported in hardware/software, can be a nightmare at universities due to IT policies)
  \item Working from the ground up. The problems of multithreadding etc. Discuss that this is one thing proprietary options such as LabView handles well (until the project scales).
  \item EPICS - For large scale systems, overly complex for smaller ones.
  \item Discuss WoT framework and LabThings initiative
  \item (There must be others?)
 \end{itemize}
 \item Front end
 \begin{itemize}
  \item Discuss frameworks for GUI applications. QT, kivy, GTK (others?).
  \item Again the issue of multithreadding are problematic. The programmer should not have to handle this explicitly to build a simple instrument.
  \item Look into EPICS front ends
  \item Discuss web-based options with client server interface (LabThings again)
 \end{itemize}
\end{itemize}


\subsection{Collaboration and Project management}

Topics to cover in prose
\begin{itemize}
 \item Collaboration on design
 \begin{itemize}
  \item Git - pros and cons for design
   \begin{itemize}
    \item Pro - For simpler text based formats, can be a great way to keep track of changes and who made them. Not all text based formats work well with git however, an example being XML.
    \item Con - Often confusing for non-coders, things like merge conflicts with hardware files can be terribly frustrating.
    \item Con - Tooling is (overall) geared towards programmers, not designers and engineers.
    \item Con - Not good with binary files often used and exported by CAD software.
   \end{itemize}
  \item GitBased collaboration platforms (GitLab, Gitea), also proprietary options such as GitHub  [Cite - \url{https://www.techrxiv.org/articles/preprint/HardOps_Utilising_the_software_development_toolchain_for_hardware_design/15052848}]
  \item Cloud storage, has issues with tracking changes effectively, and on open collaboration (NextCloud, OwnCloud, -many proprietary options)
  \item Proprietary platforms like WikiFactory and GrabCAD offer CAD tools for online-collaboration and offer options for manufacturing (open-source licenses can be selected by-design in WikiFactory). All history is stored within the platform causing possible lock-in. Open source collaborative CAD tools that provide a clear record of authorship are needed.
 \end{itemize}
 \item Project management
 \begin{itemize}
  \item Project management software becomes important only if a project grows beyond a few people in one place who maintain a ToDo list. Many ways to do project management, adopting the workflow is often the issue, not finding software
  \item GitLab (or other Git Platform) Issues. Beneficial for projects using these platforms as all in one place. GitLab has Epics, allowing Gantt charts, but this is a premium feature, available at no cost for projects on their open source program, but not available to any open repo.
  \item Open platforms for project management like Taiga exist if needed.
 \end{itemize}
\end{itemize}
\subsection{Meta tools}

Topics to cover in prose
\begin{itemize}
 \item Open software development has numerous tools to assess quality of code, and health of projects. These can help a project avoid pitfalls. Similar tools are needed for hardware.
 \item Some meta projects in their infancy for OpenSCAD
 \begin{itemize}
  \item OpenSCAD Docsgen - Documentation of the code not the hardware
  \item SCA2D - Code linter for OpenSCAD
 \end{itemize}
 \item This is becoming more of a topic of discussion in the CodeCAD (programmatic CAD) community, with using things like continuous integration to check part tolerances, assembly fit, do FEA testing, etc. Example of a recent article that touches on this topic can be found [here \url{https://medium.com/embedded-ventures/mechanical-cad-yesterday-today-and-tomorrow-981cef7e06b1}].
\end{itemize}

\section{Discussion and Future needs}


Topics to cover in prose
\begin{itemize}
 \item Institutional/funding support is needed for software. Science relies on instrumentation, and replication of that instrumentation.
 \item CAD interoperability is a problem that goes well beyond science instrumentation. It is hard to see how a specific format taking off without buy in from proprietary options. However, what is their incentive when it reduces their ability to lock in? Science instrumentation is too small a market share for open access policies to effect change.
 \item Documentation is essential for replication but poorly rewarded. Open Access could require documentation, but documentation completeness is very hard to judge. Tools for automating documentation could improve replicability of scientific instrumentation.
 \item Software for hardware control is complex to write well. Well supported frameworks are needed for this. Libraries that add features are often preferable to frameworks as they add features rather than prescribe how code is written. This is however is not really possible for something as complex as abstracting away multithreading.
 \item In some areas, usability and features of the tools are way inferior to commercial proprietary alternatives. If institutionalized and funded, the Open Hardware community might want to focus on new features, that could incentivize users of conventional tools to switch, to gain attention.
\end{itemize}

\section{Conclusion}

%Just picked a style at random, will change when we have a venue
\bibliographystyle{IEEEtran}
\bibliography{refs}
\end{document}
